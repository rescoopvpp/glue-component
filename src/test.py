import unittest
from unittest.mock import patch

import pandas as pd
from glue import Glue

from jinja2.exceptions import UndefinedError


class AbstractTestCase(unittest.TestCase):
    pass


class ParsingValidConfigTestCase(AbstractTestCase):

    def setUp(self) -> None:
        self.config_dict = {
            "ingredients": {
                "total_consumption":
                    {
                        "friendly_name": "Total energy consumed",
                        "topic_in": "shellies/shellyem-B9F2CA/emeter/1/total"},
                "total_injection":
                    {
                        "friendly_name": "Total energy injected",
                        "topic_in": "shellies/shellyem-B9F2CA/emeter/1/total_returned"},
                "total_power":
                    {
                        "friendly_name": "Total energy injected",
                        "topic_in": "shellies/shellyem-B9F2CA/emeter/1/power"},
                "voltage":
                    {
                        "friendly_name": "Grid voltage",
                        "topic_in": "shellies/shellyem-B9F2CA/emeter/1/voltage",
                        "value_template": "value_json.foo"
                    }
            },
            "recipes": {}
        }

    def test_parse_config(self) -> None:
        ingredients, recipes = Glue._parse_config(self.config_dict)

        self.assertIsInstance(ingredients, pd.DataFrame)
        self.assertEqual((4, 7), ingredients.shape, "Should have 4 rows and 7 columns")


@patch('glue.Glue.connect')
class LoadValuesFromTemplateTestCase(AbstractTestCase):
    def test_load_value_from_simple_json_payload(self, mock_connect):
        config_yaml = {
                "ingredients": {
                    "total_consumption":
                        {
                            "friendly_name": "Total energy consumed",
                            "topic_in": "dummy-device/emeter/1/total",
                            "json_template": "{{energy_consumed}}"
                        },
                },
                "recipes": {}
            }
        glue_client = Glue(config_yaml, "test_glue")

        payload = '''
        {"energy_consumed": 123}
        '''
        trigger = "dummy-device/emeter/1/total"

        value, timestamp = glue_client.load_value_from_payload(payload, trigger, 4000)

        self.assertEqual("123", value)
        self.assertEqual(4000, timestamp)

    def test_load_value_with_calculated_value(self, mock_connect):
        config_yaml = {
                "ingredients": {
                    "total_consumption":
                        {
                            "friendly_name": "Device Status",
                            "topic_in": "dummy-device/jinja",
                            "json_template": "{{test1 - test2 |int}}"
                        },
                },
                "recipes": {}
            }
        glue_client = Glue(config_yaml, "test_glue")

        payload = """{"test1": 1, "test2": 3, "test3": 7, "year": 1964}"""
        trigger = "dummy-device/jinja"

        value, timestamp = glue_client.load_value_from_payload(payload, trigger, 4000)

        self.assertEqual("-2", value)
        self.assertEqual(4000, timestamp)

    def test_load_value_when_no_template(self, mock_connect):
        config_yaml = {
                "ingredients": {
                    "total_consumption":
                        {
                            "friendly_name": "Device Status",
                            "topic_in": "dummy-device/state",
                        },
                },
                "recipes": {}
            }
        glue_client = Glue(config_yaml, "test_glue")

        payload = "on"
        trigger = "dummy-device/state"

        value, timestamp = glue_client.load_value_from_payload(payload, trigger, 4000)

        self.assertEqual("on", value)
        self.assertEqual(4000, timestamp)

    def test_load_value_when_blank_template(self, mock_connect):
        config_yaml = {
                "ingredients": {
                    "total_consumption":
                        {
                            "friendly_name": "Device Status",
                            "topic_in": "dummy-device/state",
                            "json_template": ""
                        },
                },
                "recipes": {}
            }
        glue_client = Glue(config_yaml, "test_glue")

        payload = "on"
        trigger = "dummy-device/state"

        value, timestamp = glue_client.load_value_from_payload(payload, trigger, 4000)

        self.assertEqual("on", value)
        self.assertEqual(4000, timestamp)

    def test_load_value_parses_timestamp_from_json_payload(self, mock_connect):
        config_yaml = {
                "ingredients": {
                    "total_consumption":
                        {
                            "friendly_name": "Device Status",
                            "topic_in": "dummy-device/jinja",
                            "json_template": "{{test1 - test2 |int}}"
                        },
                },
                "recipes": {}
            }
        glue_client = Glue(config_yaml, "test_glue")

        payload = """{"test1": 1, "test2": 3, "test3": 7, "year": 1964, "timestamp": 3000}"""
        trigger = "dummy-device/jinja"

        value, timestamp = glue_client.load_value_from_payload(payload, trigger, 4000)

        self.assertEqual("-2", value)
        self.assertEqual(3000, timestamp)


@patch('glue.Glue.connect')
class ValueNotInPayloadTestCase(AbstractTestCase):
    def test_load_value_fails_gracefully_if_key_not_in_payload(self, mock_connect):
        config_yaml = {
                "ingredients": {
                    "total_consumption":
                        {
                            "friendly_name": "Device Status",
                            "topic_in": "dummy-device/jinja",
                            "json_template": "{{foo}}"
                        },
                },
                "recipes": {}
            }
        glue_client = Glue(config_yaml, "test_glue")

        payload = """{"bar": 3000}"""
        trigger = "dummy-device/jinja"

        with self.assertRaises(UndefinedError):
            glue_client.load_value_from_payload(payload, trigger, 4000)
