# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 16:01:23 2020
First minimal implementation of the COFYCO glue component
@author: Joannes //Epyon// Laveyne
"""
import json
import os
import signal
import time
import logging
import paho.mqtt.client as mqtt
import pandas as pd
from pandas.core.frame import DataFrame
import yaml

from jinja2 import Template, StrictUndefined
from jinja2.exceptions import UndefinedError
from typing import Tuple, Union

MQTT_BROKER = os.getenv("MQTT_BROKER", "localhost")
dir_path = os.path.dirname(os.path.realpath(__file__))
logging.info(f"MQTT Broker is {MQTT_BROKER}")
brokers_out = {"broker1": MQTT_BROKER}

"""
When the Grim Sysadmin comes to reap with his scythe,
let this venerable daemon process die a Graceful Death
"""


class GracefulDeath:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


def to_number_if_possible(value: str) -> Union[int, float, str]:
    """
    Returns input string as an int if possible, else a float if possible, else makes no change.
    """
    try:
        return int(value)
    except ValueError:
        try:
            return float(value)
        except ValueError:
            return value


class Glue(mqtt.Client):
    MQTT_BROKER = os.getenv("MQTT_BROKER", "localhost")
    ingredients: DataFrame
    recipes: DataFrame

    def __init__(self, config: dict, client_id: str):
        mqtt.Client.__init__(self, client_id=client_id)

        self.ingredients, self.recipes = self._parse_config(config)

        # Build the list of topics to subscribe to
        sublist = self.ingredients["topic_in"].tolist()
        self.subs = []
        for sub in sublist:
            test = (sub, 0)
            self.subs.append(test)

        self.connect(brokers_out["broker1"])
        self.subscribe(self.subs)

    def on_message(self, client, userdata, message):
        logging.info(
            f"Message received : {message.payload.decode('utf-8')} on {message.topic}"
        )

        if message.topic in self.ingredients["topic_in"].values:
            self.process_mqtt_message(message)

    def load_value_from_payload(self, payload: str, topic: str, timestamp: int) -> Tuple[str, int]:
        """
        The message handler
        Checks the received message, extracts the value (template)
        If the message contains a timestamp, returns that, otherwise returns the current
        time that has been passed in.
        """

        json_template = self.ingredients.loc[self.ingredients["topic_in"] == topic][
            "json_template"
        ][0]
        if not json_template:
            value = payload
            return value, timestamp

        payload_dict = json.loads(payload)

        jinja_template = Template(json_template, undefined=StrictUndefined)
        value = jinja_template.render(payload_dict)
        try:
            timestamp = payload_dict["timestamp"]
        except KeyError:
            pass

        return value, timestamp

    def execute_recipe(self, recipe_name: str, value: str, timestamp) -> str:
        return json.dumps(
            {
                "entity": self.recipes[recipe_name]["entity"],
                "channel": self.recipes[recipe_name]["channel"],
                "value": to_number_if_possible(value),
                "unit": self.recipes[recipe_name]["unit"],
                "metric": self.recipes[recipe_name]["metric"],
                "metricKind": self.recipes[recipe_name]["metricKind"],
                "timestamp": timestamp,
            },
            ensure_ascii=False,
        )

    def process_mqtt_message(self, message):
        """Process the MQTT message."""
        payload = str(message.payload.decode("utf-8"))
        timestamp = int(time.time())

        # TODO: Handle multiple ingredients for the same topic
        # https://gitlab.com/rescoopvpp/glue-component/-/issues/2

        ingredient_name = self.ingredients.loc[
            self.ingredients["topic_in"] == message.topic
        ].index[0]

        logging.info(f"Received ingredient {ingredient_name}")

        self.ingredients.at[
            ingredient_name, "ingredient"
        ] = payload  # store raw payload in ingredients df

        try:
            value, timestamp = self.load_value_from_payload(
                payload, message.topic, timestamp
            )
        except UndefinedError:
            return

        self.ingredients.at[ingredient_name, "value"] = value
        self.ingredients.at[ingredient_name, "timestamp"] = timestamp

        print("Executing recipe(s):")
        for recipe_name in self.ingredients.at[ingredient_name, "recipes"]:
            logging.info("- " + recipe_name)
            try:
                output_value = self.execute_recipe(recipe_name, value, timestamp)
                logging.info(
                    f"Publishing to {self.recipes[recipe_name]['topic_out']} : {output_value}"
                )
                client.publish(self.recipes[recipe_name]["topic_out"], output_value)
            except Exception:
                logging.exception(f"Messed up the recipe: {recipe_name}")
                pass

    @staticmethod
    def _parse_config(config_dict: dict) -> Tuple[DataFrame, dict]:
        # Build the dataframe containing the ingredients
        INGREDIENT_COLUMNS = [
            "id",
            "friendly_name",
            "topic_in",
            "ingredient",
            "json_template",
            "value",
            "timestamp",
            "recipes",
        ]
        ingredients = pd.DataFrame(columns=INGREDIENT_COLUMNS)
        # Init the dataframe with parameters from the config file, init rest to None
        raw_ingredients = config_dict["ingredients"]
        for ingredient in raw_ingredients:
            friendly_name = ""
            json_template = ""
            if "friendly_name" in raw_ingredients[ingredient]:
                friendly_name = raw_ingredients[ingredient][
                    "friendly_name"
                ]  # optional parameter
            if "json_template" in raw_ingredients[ingredient]:
                json_template = raw_ingredients[ingredient][
                    "json_template"
                ]  # optional parameter
            df = pd.DataFrame(
                [
                    [
                        ingredient,
                        friendly_name,
                        raw_ingredients[ingredient]["topic_in"],
                        None,
                        json_template,
                        None,
                        None,
                        [],
                    ]
                ],
                columns=INGREDIENT_COLUMNS,
            )  # build temporary df
            ingredients = ingredients.append(
                df, ignore_index=True
            )  # add temp df to ingredients dataframe
        ingredients.set_index("id", inplace=True)

        # Read in the recipes
        recipes = config_dict["recipes"]
        for recipe in recipes:
            if recipes[recipe]["recipe"] in ingredients.index:
                ingredients.loc[recipes[recipe]["recipe"], "recipes"].append(recipe)

        return ingredients, recipes


"""
The main loop which checks for subcribed topics and calls the message handler
when a message arrives
"""
if __name__ == "__main__":
    """
    Read in the config file containing the glue configuration and initialise the script
    """
    with open("/cofybox-config/glue_keys.yaml", "r") as stream:
        try:
            config = yaml.safe_load(stream)
            # print(config)
        except yaml.YAMLError as exc:
            logging.exception(exc)

    client = Glue(config, "Glue")

    killer = GracefulDeath()

    while not killer.kill_now:
        try:
            client.loop_start()
            # client.loop_forever()
            client.loop_stop()
        except KeyboardInterrupt:
            client.loop_stop()
            logging.info("Unsubscribing from topics...")
            for topic in client.subs:
                client.unsubscribe(topic[0])
            logging.info("done")
            logging.info("Disconnecting from broker... ", end="")
            client.disconnect()
            logging.info("Press Ctrl-C to terminate while statement")
            pass

    # Make a graceful exit when this daemon gets terminated
    logging.info("Termination signal received")
    logging.info("Unsubscribing from topics... ", end="")
    for topic in client.subs:
        client.unsubscribe(topic[0])
    logging.info("done")
    logging.info("Disconnecting from broker... ", end="")
    client.disconnect()
    logging.info("done")
    logging.info("Goodbye, cruel Unix world")
