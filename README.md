# Glue component

Evolving implementation of the COFYCO glue component

# Purpose of the glue component
* Forward messages from proprietary topics to standard COFY ontology topics
* Reformat payloads from proprietary format to standard COFY ontology format
  * JSONify payloads
  * Add or remove attributes
* Modify payloads
  * Add, subtract, rescale payload value with fixed values or payloads of other topics
  * combine topics and their payloads into one

The glue component is implemented as a Python service which, at startup, reads in a YAML configuration file. This configuration file contains:
* a list of ingredients: the MQTT topics to subscribe to and, optional, the payload attributes to extract,
* a list of recipes: the operations to be performed with the ingredients.

# Documentation
Documentation for the Glue Component is available on the COFYbox wiki [here](https://docs.cofybox.io/e/en/cofybox/blocks/glue)